barrel_mongodb_storage
=====

A mongodb storage backend for Barrel.

Barrel can support multiple storage backend. This backend allows the user to store the data from Barrel
 in mongodb while still using the same API to retrieve changes, create views or replicate the data between multiple
locations and storages.


## Usage

The mongodb storage backend requires mongodb 4.0 and sup.


Add the storage backend as a dependency in the application embedding barrel:

```
[
...
  {barrel_mongodb_storage,
    {git, "https://gitlab.com/barrel-db/lab/barrel_mongodb_storage.git",
     {branch, "master"}}}

]
```

> **note:** Don't forget to load it before barrel.

To use it simply add to the barrel configuration file
(`sys.config`) or other the following :

```erlang
{barrel, [

    ...

    {storage, barrel_mongodb_storage}
]}
```


### Configuration


* `mongo_database` : the mongo database name, the default is "barrel_<NODENAME>".
* `mongo_seeds`: the mongodb topology

To connect a single name:
{ single, "hostname:27017" }`

To connect to a replicat set:

```{ rs, <<"ReplicaSetName">>, [ "hostname1:port1", "hostname2:port2"] }```

To connect to a sharded cluster of mongos:
```{ sharded,  ["hostname1:port1", "hostname2:port2"] }```

If you want your MongoDB deployment metadata to be auto revered use unknow id in Seed tuple:

```{ unknown,  ["hostname1:port1", "hostname2:port2"] }```

* `mongo_topology_options`

```
[
    { name,  Name },    % Name should be used for mongoc pool to be registered with
    { register,  Name },    % Name should be used for mongoc topology process to be registered with

    { pool_size, 5 }, % pool size on start
    { max_overflow, 10 },	% number of overflow workers be created, when all workers from pool are busy
    { overflow_ttl, 1000 }, % number of milliseconds for overflow workers to stay in pool before terminating
    { overflow_check_period, 1000 }, % overflow_ttl check period for workers (in milliseconds)

    { localThresholdMS, 1000 }, % secondaries only which RTTs fit in window from lower RTT to lower RTT + localThresholdMS could be selected for handling user's requests

    { connectTimeoutMS, 20000 },
    { socketTimeoutMS, 100 },

    { serverSelectionTimeoutMS, 30000 }, % max time appropriate server should be select by
    { waitQueueTimeoutMS, 1000 }, % max time for waiting worker to be available in the pool

    { heartbeatFrequencyMS, 10000 },    %  delay between Topology rescans
    { minHeartbeatFrequencyMS, 1000 },

    { rp_mode, primary }, % default ReadPreference mode - primary, secondary, primaryPreferred, secondaryPreferred, nearest

    { rp_tags, [{tag,1}] }, % tags that servers shoul be tagged by for becoming candidates for server selection  (may be an empty list)
]
```


* `mongo_worker_options`: describing how mongodb connect

```
{login, binary()}
{password, binary()}
{w_mode, write_mode()}.
```

## Build

    $ rebar3 compile

## Screenshot

![screenshot](doc/screenshot.png "storage of the data in mongodb")


## Author

Barrel DB is a made by [Enki Multimedia](https://www.enki-multimedia.eu) a company founded by Benoît Chesneau.

