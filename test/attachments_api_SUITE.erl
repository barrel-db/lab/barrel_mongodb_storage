%%% -*- erlang -*-
%%%
%%% this file is part of barrel_mongodb_storage released under the apache 2 license.
%%% see the notice for more information.
%%%
-module(attachments_api_SUITE).
-author("benoitc").

%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/2
]).

-export([
  basic/1
]).

all() ->
  [
    basic
  ].

init_per_suite(Config) ->
  _ = application:load(barrel),
  application:set_env(barrel, storage, barrel_mongodb_storage),
  {ok, _} = application:ensure_all_started(barrel_mongodb_storage),
  Config.


init_per_testcase(_, Config) ->
  ok = barrel_db:create_barrel(<<"test">>),
  Config.

end_per_testcase(_, _Config) ->
  ok = barrel_db:delete_barrel(<<"test">>),
  ok.

end_per_suite(Config) ->

  Config.


basic(_Config) ->
  {ok,  Barrel}= barrel_db:open_barrel(<<"test">>),
  Doc0 = #{ <<"id">> => <<"a">>,
            <<"v">> => 1,
            <<"_attachments">> => #{
                <<"att">> => #{ <<"data">> => <<"test">> }
               }
          },
  {ok, <<"a">>, Rev} = barrel:save_doc(Barrel, Doc0),
  {ok, #{ <<"_rev">> := Rev,
          <<"_attachments">> := #{
              <<"att">> := #{ 
                             <<"data">> := <<"test">>
                            }
             }
        } = Doc1 } = barrel:fetch_doc(Barrel, <<"a">>, #{ attachments => true }),

  {ok, #{ <<"_rev">> := Rev,
          <<"_attachments">> := #{
              <<"att">> := #{ 
                             <<"stub">> := true
                            }
             }
        } } = barrel:fetch_doc(Barrel, <<"a">>, #{ attachments => false }),


   {ok, <<"a">>, Rev2} = barrel:save_doc(Barrel, Doc1#{ <<"v">> => 2 }),
   {ok, #{ <<"_rev">> := Rev2} } = _Doc2 = barrel:fetch_doc(Barrel, <<"a">>, #{}),

   {ok, <<"test">>} = barrel:fetch_attachment(Barrel, <<"a">>, <<"att">>),
   ok.
