%%% -*- erlang -*-
%%%
%%% this file is part of barrel_mongodb_storage released under the apache 2 license.
%%% see the notice for more information.
%%%
-module(barrel_api_SUITE).
-author("benoitc").

%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/2
]).

-export([
  create_barrel/1,
  list_barrels/1
]).

all() ->
  [
    create_barrel,
    list_barrels
  ].

init_per_suite(Config) ->
  _ = application:load(barrel),
  application:set_env(barrel, storage, barrel_mongodb_storage),
  {ok, _} = application:ensure_all_started(barrel_mongodb_storage),
  Config.


init_per_testcase(_, Config) ->
  Config.

end_per_testcase(_, _Config) ->
  ok.

end_per_suite(Config) ->
  Config.


create_barrel(_Config) ->
  ok = barrel_db:create_barrel(<<"test">>),
  {error, barrel_already_exists} = barrel_db:create_barrel(<<"test">>),
  {ok, _Barrel} = barrel_db:open_barrel(<<"test">>),
  ok = barrel_db:delete_barrel(<<"test">>),
  {error, barrel_not_found} = barrel_db:open_barrel(<<"test">>),
  ok = barrel_db:create_barrel(<<"test">>),
  ok = barrel_db:delete_barrel(<<"test">>),
  ok.


list_barrels(_Config) ->
  [] =  barrel:all_names(),
  ok = barrel_db:create_barrel(<<"test">>),
  [<<"test">>] = barrel:all_names(),
  ok = barrel:create_barrel(<<"test1">>),
  [<<"test">>, <<"test1">>] = barrel:all_names(),
  ok = barrel:delete_barrel(<<"test1">>),
  [<<"test">>] = barrel:all_names(),
  ok = barrel:delete_barrel(<<"test">>),
  [] =  barrel:all_names(),
  ok.

