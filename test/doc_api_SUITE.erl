%%% -*- erlang -*-
%%%
%%% this file is part of barrel_mongodb_storage released under the apache 2 license.
%%% see the notice for more information.
%%%
-module(doc_api_SUITE).
-author("benoitc").

%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/2
]).

-export([
  basic/1,
  update/1,
  update_docs/1,
  delete_doc/1,
  fold_docs/1,
  infos/1,
  update_non_existing_doc/1
]).

all() ->
  [
    basic,
    update,
    update_docs,
    delete_doc,
    fold_docs,
    infos,
    update_non_existing_doc
  ].

init_per_suite(Config) ->
  _ = application:load(barrel),
  application:set_env(barrel, storage, barrel_mongodb_storage),
  {ok, _} = application:ensure_all_started(barrel_mongodb_storage),
  Config.


init_per_testcase(_, Config) ->
  ok = barrel_db:create_barrel(<<"test">>),
  Config.

end_per_testcase(_, _Config) ->
  ok = barrel_db:delete_barrel(<<"test">>),
  ok.

end_per_suite(Config) ->

  Config.




basic(_Config) ->
  {ok,  Barrel}= barrel_db:open_barrel(<<"test">>),
  Doc0 = #{ <<"id">> => <<"a">>, <<"v">> => 1},
  {ok, <<"a">>, Rev} = barrel:save_doc(Barrel, Doc0),
  {ok, #{ <<"_rev">> := Rev, <<"v">> := 1 }} = barrel:fetch_doc(Barrel, <<"a">>, #{}),
  ok.


update(_Config) ->
  {ok,  Barrel}= barrel_db:open_barrel(<<"test">>),
  Doc0 = #{ <<"id">> => <<"a">>, <<"v">> => 1},
  {ok, <<"a">>, Rev} = barrel:save_doc(Barrel, Doc0),
  {ok, #{ <<"_rev">> := Rev,
          <<"v">> := 1 } = Doc1} = barrel:fetch_doc(Barrel, <<"a">>, #{}),
  {ok, <<"a">>, Rev1} = barrel:save_doc(Barrel, Doc1#{<<"v">> => 2 }),
  true = (Rev1 =/= Rev),
  {ok, #{ <<"_rev">> := Rev1, <<"v">> := 2 }} = barrel:fetch_doc(Barrel, <<"a">>, #{}),
  ok.


update_docs(_Config) ->
  {ok,  Barrel}= barrel_db:open_barrel(<<"test">>),
  Docs = [
    #{ <<"id">> => <<"a">>, <<"v">> => 1},
    #{ <<"id">> => <<"b">>, <<"v">> => 1}
  ],
  {ok, [
    {ok, <<"a">>, _Rev1},
    {ok, <<"b">>, _Rev2}
  ]} = barrel:save_docs(Barrel, Docs),
  ok.

delete_doc(_Config) ->
  Doc0 = #{ <<"id">> => <<"a">>, <<"v">> => 1},
  {ok,  Barrel}= barrel_db:open_barrel(<<"test">>),
  {ok, <<"a">>, Rev} = barrel:save_doc(Barrel, Doc0),
  {ok, #{ <<"_rev">> := Rev }} = barrel:fetch_doc(Barrel, <<"a">>, #{}),
  {ok, <<"a">>, Rev2} = barrel:delete_doc(Barrel, <<"a">>, Rev),
  {error, not_found} = barrel:fetch_doc(Barrel, <<"a">>, #{}),
  {ok, Doc2} = barrel:fetch_doc(Barrel, <<"a">>, #{ rev => Rev2}),
  undefined = maps:get(<<"v">>, Doc2, undefined),
  {ok, <<"a">>, Rev_1} = barrel:save_doc(Barrel, Doc0),
  true = (Rev =/= Rev_1),
  {ok, #{ <<"_rev">> := Rev_1, <<"v">> := 1 }} = barrel:fetch_doc(Barrel, <<"a">>, #{}),
  ok.

fold_docs(_Config) ->
  Docs = [
    #{ <<"id">> => <<"a">>, <<"v">> => 1},
    #{ <<"id">> => <<"b">>, <<"v">> => 2},
    #{ <<"id">> => <<"c">>, <<"v">> => 3},
    #{ <<"id">> => <<"d">>, <<"v">> => 4},
    #{ <<"id">> => <<"e">>, <<"v">> => 5}
  ],
  {ok,  Barrel}= barrel_db:open_barrel(<<"test">>),
  {ok, _Saved} = barrel:save_docs(Barrel, Docs),
  5 = length(_Saved),
  Fun = fun(#{ <<"id">> := Id }, Acc) -> {ok, [ Id | Acc ]} end,
  Result1 = barrel:fold_docs(Barrel, Fun, [], #{}),
  [<<"a">>, <<"b">>, <<"c">>, <<"d">>, <<"e">>] = lists:reverse(Result1),
  {ok, #{ <<"_rev">> := RevC}} = barrel:fetch_doc(Barrel, <<"c">>, #{}),
  {ok, _, _} = barrel:delete_doc(Barrel, <<"c">>, RevC),
  {error, not_found} = barrel:fetch_doc(Barrel, <<"c">>, #{}),
  Result2 = barrel:fold_docs(Barrel, Fun, [], #{}),
  [<<"a">>, <<"b">>, <<"d">>, <<"e">>] = lists:reverse(Result2),
  Result3 = barrel:fold_docs(Barrel, Fun, [], #{include_deleted => true}),
  [<<"a">>, <<"b">>, <<"c">>, <<"d">>, <<"e">>] = lists:reverse(Result3),
  ok.

infos(_Config) ->
  {ok,  #{ uid := UID } = Barrel}= barrel_db:open_barrel(<<"test">>),
  {ok, #{ uid := UID,
          docs_count := 0,
          docs_del_count := 0 }} = barrel:barrel_infos(<<"test">>),
  Docs = [
          #{ <<"id">> => <<"a">>, <<"v">> => 1},
          #{ <<"id">> => <<"b">>, <<"v">> => 2},
          #{ <<"id">> => <<"c">>, <<"v">> => 3},
          #{ <<"id">> => <<"d">>, <<"v">> => 4},
          #{ <<"id">> => <<"e">>, <<"v">> => 5}
         ],
  {ok, _Saved} = barrel:save_docs(Barrel, Docs),
  5 = length(_Saved),
  {ok, #{ uid := UID,
          docs_count := 5,
          docs_del_count := 0 }} = barrel:barrel_infos(<<"test">>),
  ok.

update_non_existing_doc(_Config) ->
  {ok,  Barrel}= barrel_db:open_barrel(<<"test">>),
  Doc0 = #{ <<"id">> => <<"a">>, <<"v">> => 1, <<"_rev">> => <<"1-76d70d853c9fcf1f83a6b4b6cf3776633d28f480cb0dd7ee8b68d5bfc434360a">>},
  {error, {conflict, revision_conflict}}  = barrel:save_doc(Barrel, Doc0).
