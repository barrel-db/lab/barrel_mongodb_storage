%%% -*- erlang -*-
%%%
%%% this file is part of barrel_mongodb_storage released under the apache 2 license.
%%% see the notice for more information.
%%%
-module(changes_api_SUITE).

-author("benoitc").

%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/2
]).

-export(
   [
    fold_changes/1,
    stream/1
   ]
  ).

all() ->
  [
   fold_changes,
   stream
  ].


init_per_suite(Config) ->
  _ = application:load(barrel),
  application:set_env(barrel, storage, barrel_mongodb_storage),
  {ok, _} = application:ensure_all_started(barrel_mongodb_storage),
  Config.


init_per_testcase(_, Config) ->
  ok = barrel_db:create_barrel(<<"test">>),
  Config.

end_per_testcase(_, _Config) ->
  ok = barrel_db:delete_barrel(<<"test">>),
  ok.

end_per_suite(Config) ->

  Config.

fold_changes(_Config) ->
  {ok,  Barrel}= barrel_db:open_barrel(<<"test">>),
  Docs = [
    #{ <<"id">> => <<"a">>, <<"v">> => 1},
    #{ <<"id">> => <<"b">>, <<"v">> => 2},
    #{ <<"id">> => <<"c">>, <<"v">> => 3},
    #{ <<"id">> => <<"d">>, <<"v">> => 4},
    #{ <<"id">> => <<"e">>, <<"v">> => 5}
  ],
  {ok, _Saved} = barrel:save_docs(Barrel, Docs),
  5 = length(_Saved),
  Fun = fun(Change, Acc) -> {ok, [ Change | Acc ]} end,
  {ok, Changes1, LastSeq1} = barrel:fold_changes(Barrel, first, Fun, [], #{}),
  5 = length(Changes1),
  {_, SeqBin} = barrel_sequence:from_string(LastSeq1),
  {_, 5} = barrel_sequence:decode(SeqBin),
  [#{ <<"id">> := <<"a">> },
   #{ <<"id">> := <<"b">> },
   #{ <<"id">> := <<"c">> },
   #{ <<"id">> := <<"d">> },
   #{ <<"id">> := <<"e">> }] = lists:sort(fun(#{ <<"seq">> := S1 }, #{<<"seq">> := S2}) ->
                                            (S1 =< S2)
                                        end,
                                        Changes1),
  {ok, #{ <<"_rev">> := RevC}} = barrel:fetch_doc(Barrel, <<"c">>, #{}),
  {ok, _, _} = barrel:delete_doc(Barrel, <<"c">>, RevC),
  {error, not_found} = barrel:fetch_doc(Barrel, <<"c">>, #{}),
  {ok, Changes2, LastSeq2} = barrel:fold_changes(Barrel, LastSeq1, Fun, [], #{}),
  [#{ <<"id">> := <<"c">> }] = lists:reverse(Changes2),
  {_, SeqBin2} =  barrel_sequence:from_string(LastSeq2),
  {_, 6} = barrel_sequence:decode(SeqBin2),

  {ok, [], LastSeq2} = barrel:fold_changes(Barrel, LastSeq2, Fun, [], #{include_deleted => true}),
  ok.

stream(_Config) ->
  {ok,  Barrel}= barrel_db:open_barrel(<<"test">>),
  Docs = [
          #{ <<"id">> => <<"a">>, <<"v">> => 1},
          #{ <<"id">> => <<"b">>, <<"v">> => 2},
          #{ <<"id">> => <<"c">>, <<"v">> => 3},
          #{ <<"id">> => <<"d">>, <<"v">> => 4},
          #{ <<"id">> => <<"e">>, <<"v">> => 5}
         ],
  {ok, _Saved} = barrel:save_docs(Barrel, Docs),
  5 = length(_Saved),
  Fun = fun(Change, Acc) -> {ok, [ Change | Acc ]} end,
  {ok, Changes, _LastSeq} = barrel:fold_changes(Barrel, first, Fun, [], #{}),
  5 = length(Changes),

  {ok, StreamPid} = barrel_changes_stream:start_link(<<"test">>, self(), #{}),
  {ReqId, Changes_1} = barrel_changes_stream:await(StreamPid),
  ok = barrel_changes_stream:ack(StreamPid, ReqId),

  true = (Changes_1 =:= Changes),
  ok.

