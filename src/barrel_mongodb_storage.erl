%%% -*- erlang -*-
%%%
%%% this file is part of barrel_mongodb_storage released under the apache 2 license.
%%% see the notice for more information.
%%%
-module(barrel_mongodb_storage).


-export([create_barrel/1,
         open_barrel/1,
         delete_barrel/1,
         fold_names/2,
         last_updated_seq/1,
         barrel_infos/1,
         get_doc_info/2,
         get_doc_revision/3,
         insert_doc/4,
         update_doc/6,
         fold_docs/4,
         fold_changes/4,
         changes_iterator/2,
         changes_next/1,
         close_changes_iterator/1,
         open_view/3,
         update_indexed_seq/2,
         update_view_index/3,
         fold_view_index/5,
         get_counter/2,
         set_counter/3,
         add_counter/3,
         delete_counter/2]).

-export([init_ctx/2,
         release_ctx/1]).

%% public gen_server api
-export([start_link/0]).

%% internal
-export([status/0]).

%% gen server API
-export([init/1,
         handle_call/3,
         handle_cast/2,
         terminate/2]).


-include_lib("barrel/include/barrel.hrl").

-define(mongodb, persistent_term:get({?MODULE, topology})).

-define(metadata, <<"barrel_meta">>).
-define(counters, <<"barrel_counters">>).


create_barrel(Name) ->
    mongoc:transaction(
    ?mongodb,
    fun(#{pool := Worker}) ->
        case mc_worker_api:find_one(Worker, ?metadata, #{ <<"name">> => Name }) of
          undefined ->
            UID = barrel_lib:make_uid(Name),
            MetaDoc = #{ <<"name">> => Name,
                         <<"uid">> => UID },
            case mc_worker_api:insert(Worker, ?metadata, MetaDoc) of
              {{true, _}, _} ->
                mc_worker_api:ensure_index(Worker, UID, #{ <<"key">> => #{ <<"id">> => 1,
                                                                          <<"_doc_type">> => 1 }}),

                mc_worker_api:ensure_index(Worker, UID, #{  <<"key">> => #{<<"seq">> => 1,
                                                                           <<"_doc_type">> => <<"docinfo">> }}),

                mc_worker_api:ensure_index(Worker, UID, #{  <<"key">> => #{<<"key">> => 1,
                                                                           <<"_doc_type">> => 1 }}),

                ok;
              {{false, ErrorMap}, _}  -> {error, ErrorMap}
            end;
          Meta when is_map(Meta) ->
            {error, barrel_already_exists};
          Error ->
            Error
        end
    end).


open_barrel(Name) ->
  case mongo_api:find_one(?mongodb, ?metadata, #{ <<"name">> => Name }, #{}) of
    undefined ->
      {error, barrel_not_found};
    #{ <<"uid">> := UID } ->
      {ok, UID, UID}
  end.

delete_barrel(Name) ->
  mongoc:transaction(
    ?mongodb,
    fun(#{pool := Worker}) ->
        case mc_worker_api:find_one(Worker, ?metadata, #{ <<"name">> => Name }) of
          undefined -> ok;
          #{ <<"uid">> := UID } ->
            _ = mc_worker_api:delete(Worker, ?metadata, #{ <<"name">> => Name }),
            %%_ = mc_worker_api:delete(Worker, UID, #{}),
            _ = mc_worker_api:command(Worker,{<<"drop">>, UID}),
            ok
        end
    end).


fold_names(Fun, Acc) ->
  case mongo_api:find(?mongodb, ?metadata, #{ <<"name">> => #{ <<"$exists">> => true } }, #{}) of
    {ok, Cursor} ->
      try fold_names_loop(mc_cursor:next(Cursor), Cursor, Fun, Acc)
      after mc_cursor:close(Cursor)
      end;
    _ ->
      Acc
  end.


fold_names_loop({#{ <<"name">> := Name }}, Cursor, Fun, Acc) ->
  case Fun(Name, Acc) of
    {ok, Acc1} ->
      fold_names_loop(mc_cursor:next(Cursor), Cursor, Fun, Acc1);
    ok ->
      fold_names_loop(mc_cursor:next(Cursor), Cursor, Fun, Acc);
    {stop, Acc1} ->
      Acc1;
    stop ->
      Acc
  end;
fold_names_loop(_Else, _, _, Acc) ->
  Acc.


last_updated_seq(BarrelId) ->
  Selector = #{ <<"query">> => #{ <<"_doc_type">> => <<"docinfo">>},
                <<"$orderby">> => #{ <<"seq">> => -1 }},

  LastSeq = case mongo_api:find_one(?mongodb, BarrelId, Selector, #{}) of
              undefined -> barrel_sequence:sequence_min();
              #{ <<"seq">> :=  Seq } -> Seq
            end,
  barrel_sequence:to_string(LastSeq).

barrel_infos(Name) ->
  case mongo_api:find_one(?mongodb, ?metadata, #{ <<"name">> => Name }, #{}) of
    undefined ->
      {error, barrel_not_found};
    #{ <<"uid">> := UID } ->
      DocsCount = mongo_api:count(?mongodb, UID, #{ <<"_doc_type">> => <<"docinfo">>,
                                                    <<"deleted">> => false }, -1),
      DocsDelCount = mongo_api:count(?mongodb, UID, #{ <<"_doc_type">> => <<"docinfo">>,
                                                       <<"deleted">> => true }, -1),
      {ok, #{ uid => UID,
              updated_seq => last_updated_seq(UID),
              docs_count => DocsCount,
              docs_del_count => DocsDelCount }}
  end.


%% TODO: support snapshot transactions but need a change in the mongodb driver.
init_ctx(BarrelId, _IsRead) ->
  {ok, BarrelId}.

release_ctx(_) ->
  ok.

get_doc_info(BarrelId, DocId) ->
  case mongo_api:find_one(?mongodb, BarrelId, #{ <<"id">> => DocId,
                                                 <<"_doc_type">> => <<"docinfo">> }, #{}) of
    undefined ->
      {error, not_found};
    Doc ->
      {ok, to_docinfo(Doc)}
  end.

get_doc_revision(BarrelId, DocId, Rev) ->
  Selector = #{ <<"id">> => DocId,
               <<"rev">> => Rev,
               <<"_doc_type">> => <<"revision">> },
  case mongo_api:find_one(?mongodb, BarrelId, Selector, #{}) of
    undefined ->
      {error, not_found};
    #{ <<"body">> := Body } ->
      {ok, Body}
  end.


insert_doc(BarrelId, DI, DocRev, DocBody) ->
  mongoc:transaction(
    ?mongodb,
    fun(#{pool := Worker}) ->
         {{true, _}, _} = mc_worker_api:insert(Worker, BarrelId,
                                               [revision_doc(DocRev, DocBody),
                                                from_docinfo(DI)],
                                               #{ <<"w">> => <<"majority">>, <<"j">> => 1 }),
         ok
    end).


update_doc(BarrelId, #{ id := DocId } = DI, DocRev, DocBody, _OldSeq, _OldDel) ->
  mongoc:transaction(
    ?mongodb,
    fun(#{pool := Worker}) ->
        case mc_worker_api:insert(Worker, BarrelId, [revision_doc(DocRev, DocBody)]) of
          {{true, _}, _} ->
            case mc_worker_api:update(Worker, BarrelId,
                                      #{ <<"id">> => DocId,
                                         <<"_doc_type">> => <<"docinfo">> },
                                      #{ <<"$set">> => from_docinfo(DI) },
                                      true,
                                      false,
                                      #{ <<"w">> => <<"majority">>, <<"j">> => 1 }) of
              {true, _} ->
                ok;
              {false, Error} ->
                {error, Error}
            end;
          {{false, _}, Error} ->
            {error, Error}
        end
    end).

from_docinfo(DI) ->
  maps:fold(fun
              (id, ID, D) -> D#{ <<"id">> => ID };
              (rev, Rev, D) -> D#{ <<"rev">> => Rev };
              (seq, Seq, D) -> D#{ <<"seq">> => Seq };
              (deleted, Del, D) -> D#{ <<"deleted">> => Del };
              (revtree, RevTree, D) -> D#{ <<"revtree">> => term_to_binary(RevTree) }
            end, #{ <<"_doc_type">> => <<"docinfo">> }, DI).

to_docinfo(Doc) ->
  maps:fold(fun
               (<<"id">>, Id, D) -> D#{ id => Id };
               (<<"rev">>, Rev, D) -> D#{ rev => Rev };
               (<<"seq">>, Seq, D) -> D#{ seq => Seq };
               (<<"deleted">>, Del, D) -> D#{ deleted => Del };
               (<<"revtree">>, RevTree, D) -> D#{ revtree => binary_to_term(RevTree) };
               (_, _, D) -> D
             end, #{}, Doc).


revision_doc(DocRev, #{<< "id">> := DocId } = DocBody) ->
  #{ <<"id">> => DocId,
     <<"rev">> => DocRev,
     <<"body">> => DocBody,
     <<"_doc_type">> => <<"revision">>}.

%% TODO : upgrade mongo client to use latest sort api
fold_docs(BarrelId, UserFun, UserAcc, Options) ->
  Query = maps:fold(fun(next_to, NextTo, S) ->
                           S#{ <<"id">> => #{ <<"$gt">> => NextTo }};
                          (start_at, StartAt, S) ->
                           S#{ <<"id">> => #{ <<"$gte">> => StartAt }};
                          (previous_to, PreviousTo, S) ->
                           S#{ <<"id">> => #{ <<"$lt">> => PreviousTo }};
                          (end_at, EndAt, S) ->
                           S#{ <<"id">> => #{ <<"$lte">> => EndAt}};
                          (_, _, S) ->
                           S
                       end, #{ <<"_doc_type">> => <<"docinfo">> }, Options),

  Selector = #{<<"$query">> => Query,
               <<"$orderby">> => #{<<"id">> => 1} },


  case mongo_api:find(?mongodb, BarrelId, Selector, #{}) of
    {ok, Cursor} ->
      fold_docs_loop(mc_cursor:next(Cursor), Cursor, UserFun, UserAcc);
    [] ->
      UserAcc
  end.

fold_docs_loop({#{ <<"id">> := DocId }=DI}, Cursor, Fun, Acc) ->
  case Fun(DocId, to_docinfo(DI), Acc) of
    {ok, Acc1} ->
      fold_docs_loop(mc_cursor:next(Cursor), Cursor, Fun, Acc1);
    {stop, Acc1} ->
      Acc1;
    stop ->
      Acc;
    skip ->
      fold_docs_loop(mc_cursor:next(Cursor), Cursor, Fun, Acc);
    ok ->
      fold_docs_loop(mc_cursor:next(Cursor), Cursor, Fun, Acc)
  end;
fold_docs_loop(_Else, _, _, Acc) ->
  Acc.


fold_changes(BarrelId, Since, UserFun, UserAcc) ->
  Query = #{ <<"seq">> => #{ <<"$gte">> => Since },
             <<"_doc_type">> => <<"docinfo">> },

  Selector = #{<<"$query">> => Query,
               <<"$orderby">> => #{<<"seq">> => 1} },
 case mongo_api:find(?mongodb, BarrelId, Selector, #{}) of
   {ok, Cursor} ->
     fold_docs_loop(mc_cursor:next(Cursor), Cursor, UserFun, UserAcc);
   [] ->
     UserAcc
 end.

changes_iterator(BarrelId, Since) ->
  Query = #{ <<"seq">> => #{ <<"$gte">> => Since },
             <<"_doc_type">> => <<"docinfo">> },

  Selector = #{<<"$query">> => Query,
               <<"$orderby">> => #{<<"seq">> => 1} },

  case mongo_api:find(?mongodb, BarrelId, Selector, #{}) of
    {ok, Cursor} -> {ok, Cursor};
    [] -> invalid
  end.

changes_next(Cursor) ->
  case mc_cursor:next(Cursor) of
    DI when is_map(DI) ->
      {ok, to_docinfo(DI), Cursor};
    _ ->
      invalid
  end.

close_changes_iterator(Cursor) ->
  mc_cursor:close(Cursor).


open_view(BarrelId, ViewId, Version) ->
  case mongo_api:find_one(?mongodb, BarrelId, #{ <<"id">> => ViewId,
                                                 <<"_doc_type">> => <<"view">> }, #{}) of
    undefined ->
      ViewDoc = #{ <<"id">> => ViewId,
                   <<"version">> => Version,
                   <<"indexed_seq">> => barrel_sequence:sequence_min(),
                   <<"_doc_type">> => <<"view">> },
      {{true, _}, _} = mongo_api:insert(?mongodb,BarrelId, ViewDoc),
      {ok, {BarrelId, ViewId}, first, Version};
    #{ <<"version">> := OldVersion } ->
      ViewDoc = #{ <<"id">> => ViewId,
                   <<"version">> => Version,
                   <<"indexed_seq">> => barrel_sequence:sequence_min(),
                   <<"_doc_type">> => <<"view">> },
      {{true, _}, _} = mongo_api:insert(?mongodb,BarrelId, ViewDoc),
      {ok, {BarrelId, ViewId}, first, OldVersion};
    #{ <<"indexed_seq">> := IndexedSeq } ->
      {ok, {BarrelId, ViewId}, IndexedSeq, Version}
  end.

update_indexed_seq({BarrelId, ViewId}, Seq) ->
  Command = #{ <<"$set">> => #{ <<"indexed_seq">> => Seq } },
  case mongo_api:update(?mongodb, BarrelId, #{ <<"id">> => ViewId,
                                               <<"_doc_type">> => <<"view">> },
                        Command, #{ upsert => true }) of
    {true, _} ->
      ok;
    {false, Error} ->
      {error, Error}
  end.


view_revmap_key(ViewId, DocId) ->
  << ViewId/binary,  "r", DocId/binary >>.

encode_view_key(Key, AccBin) ->
  encode_view_key_1(Key, << AccBin/binary, "i" >>).

encode_view_key_1([Term|Rest], AccBin) ->
  encode_view_key_1(Rest, encode_view_term(Term, AccBin));
encode_view_key_1([], AccBin) ->
  AccBin.


encode_view_term(L, B) when is_atom(L) ->
  barrel_encoding:encode_literal_ascending(B, L);
encode_view_term(S, B) when is_binary(S) ->
  barrel_encoding:encode_binary_ascending(B, S);
encode_view_term(N, B) when is_integer(N) ->
  barrel_encoding:encode_varint_ascending(B, N);
encode_view_term(N, B) when is_number(N) ->
  barrel_encoding:encode_float_ascending(B, N).


decode_view_key(Prefix, Bin) ->
  case binary:split(Bin, << Prefix/binary, "i" >>) of
    [_ViewPrefix, KeyBin] ->
      decode_view_key_1(KeyBin, []);
    _Else ->
      erlang:error(badarg)
  end.

decode_view_key_1(<<>>, Acc) ->
  Acc;
decode_view_key_1(Bin, Acc) ->
  case barrel_encoding:pick_encoding(Bin) of
    bytes ->
      {Val, Rest} = barrel_encoding:decode_binary_ascending(Bin),
      decode_view_key_1(Rest, [Val | Acc]);
    int ->
       {Val, Rest} = barrel_encoding:decode_varint_ascending(Bin),
       decode_view_key_1(Rest, [Val | Acc]);
    float ->
      {Val, Rest} = barrel_encoding:decode_float_ascending(Bin),
      decode_view_key_1(Rest, [Val | Acc]);
    literal ->
      {Val, Rest} = barrel_encoding:decode_literal_ascending(Bin),
      decode_view_key_1(Rest, [Val | Acc]);
    _Else ->
      erlang:error(badarg)
  end.


next_term(I) when is_integer(I) ->
  I + 1;
next_term(true) ->
  false;
next_term(false) ->
  true;
next_term(null) ->
  <<>>;
next_term(B) when is_binary(B) ->
  barrel_rocksdb_util:bytes_prefix_end(B).

next_key(Key) ->
  [End|R] = lists:reverse(Key),
  lists:reverse([next_term(End) | R]).

update_view_index({BarrelId, ViewId}, DocId, KVs) ->
  RevMapKey = view_revmap_key(ViewId, DocId),
  OldReverseMap =  case mongo_api:find_one(?mongodb, BarrelId, #{ <<"id">> => RevMapKey,
                                                                  <<"_doc_type">> => <<"revmap">>}, #{}) of
                     undefined ->
                       [];
                     #{ <<"keys">> := Keys } ->
                       Keys
                   end,

  ReverseMap = lists:foldl(fun({K0, V0}, Acc) ->
                                K1 = encode_view_key(K0, ViewId),

                                Doc = #{ <<"key">> => K1,
                                         <<"value">> => V0,
                                         <<"doc_id">> => DocId,
                                         <<"_doc_type">> => <<"view_item">> },
                                {true, _} = mongo_api:update(?mongodb, BarrelId,
                                                              #{ <<"key">> => K1, <<"doc_id">> => DocId },
                                                              #{ <<"$set">> => Doc },
                                                              #{ upsert => true }),
                                [K1 | Acc]
                            end, [], KVs),
  ReverseMap1 = lists:usort(ReverseMap),
  ToDelete = OldReverseMap -- ReverseMap1,

  Deletes = lists:foldl(fun(K, Acc) ->
                            [#{ <<"q">> => #{ <<"key">> => K,
                                              <<"doc_id">> => DocId }} | Acc]
                        end, [], ToDelete),

  ReverseMapDoc = #{ <<"id">> => RevMapKey,
                      <<"keys">> => ReverseMap1,
                      <<"_doc_type">> => <<"revmap">> },

  mongoc:transaction(
    ?mongodb,
    fun(#{pool := Worker}) ->
        {true, _} = mongo_worker_api:command(db_name(), Worker, {<<"delete">>, BarrelId, <<"deletes">>, Deletes}),
        ok
    end),
  {true, _} = mongo_api:update(?mongodb, BarrelId, #{ <<"key">> => RevMapKey },
                               #{ <<"$set">> => ReverseMapDoc }, #{upsert => true }),
  ok.


fold_view_index(BarrelId, ViewId, UserFun, UserAcc, Options) ->
  Gt = case maps:get(begin_or_equal, Options, true) of
         true ->
           <<"$gte">>;
         false ->
           <<"$gt">>
       end,
  Begin = maps:get(begin_key, Options, [<<>>]),
  End = maps:get(end_key, Options, next_key(Begin)),
  {Lt, End1} = case maps:get(end_or_equal, Options, true) of
                 true ->
                   {<<"$lt">>,next_key(End)};
                 false ->
                   {<<"$lt">>, End}
               end,
  Reverse = maps:get(reverse, Options, false),
  Limit = maps:get(limit, Options, 1 bsl 64 - 1),
  Query = #{ <<"key">> => #{ Gt => encode_view_key(Begin, ViewId),
                             Lt => encode_view_key(End1, ViewId) },
             <<"_doc_type">> => <<"view_item">> },
  OrderBy = case Reverse of
              true ->
                #{ <<"key">> => -1 };
              false ->
                 #{ <<"doc_id">> => 1 }
            end,
  Selector = #{ <<"query">> => Query,
                <<"$orderby">> => OrderBy },
  WrapperFun = fun(#{ <<"doc_id">> := DocId,
                      <<"key">> := KeyBin,
                      <<"value">> := Val }, Acc) ->
                   Key = decode_view_key(ViewId, KeyBin),
                   UserFun({DocId, Key, Val}, Acc)
               end,
  case mongo_api:find(?mongodb, BarrelId, Selector, #{}) of
    {ok, Cursor} ->
      try fold_view_loop(mc_cursor:next(Cursor), Cursor, WrapperFun, UserAcc, Limit)
      after
        _ = (catch mc_cursor:close(Cursor))
      end;
    [] ->
      UserAcc
  end.


fold_view_loop({Doc}, Cursor, Fun, Acc, Limit) when Limit > 0 ->
  case Fun(Doc, Acc) of
    {ok, Acc1} ->
      fold_view_loop(mc_cursor:next(Cursor), Cursor, Fun, Acc1, Limit -1);
    {skip, Acc1} ->
      fold_view_loop(mc_cursor:next(Cursor), Cursor, Fun, Acc1, Limit);

    {stop, Acc1} ->
      Acc1;
    stop ->
      Acc;
    skip ->
      fold_view_loop(mc_cursor:next(Cursor), Cursor, Fun, Acc, Limit);
    ok ->
      fold_view_loop(mc_cursor:next(Cursor), Cursor, Fun, Acc, Limit -1)
  end;
fold_view_loop(_Else, _, _, Acc, _) ->
  Acc.

counter_key(Prefix, Name) ->
   barrel_encoding:encode_binary_ascending(Prefix, Name).

get_counter(Prefix, Name) ->
  CounterKey = counter_key(Prefix, Name),
  case mongo_api:find_one(?mongodb, ?counters, #{ <<"key">> => CounterKey }, #{}) of
    undefined -> not_found;
    #{ <<"val">> := Val } -> {ok, Val}
  end.

set_counter(Prefix, Name, Value) ->
  CounterKey = barrel_rocksdb_keys:counter_key(Prefix, Name),
  {{true, _}, _} = mongo_api:insert(?mongodb, ?counters,
                                     #{ <<"key">> => CounterKey,
                                        <<"val">> => Value}),
  ok.


add_counter(Prefix, Name, Value) ->
  CounterKey = barrel_rocksdb_keys:counter_key(Prefix, Name),
  {true, _} = mongo_api:update(?mongodb, ?counters,
                                #{ <<"">> => CounterKey},
                                #{ <<"$inc">> => Value },
                                #{ upsert => true }),
  ok.
delete_counter(Prefix, Name) ->
  CounterKey = barrel_rocksdb_keys:counter_key(Prefix, Name),
  {true, _} = mongo_api:delete(?mongodb, ?counters, #{ <<"key">> => CounterKey}),
  ok.


status() ->
  mongoc:status(?mongodb).

start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

db_name() ->
  <<"barrel_", (barrel_lib:to_binary(node()))/binary >>.

init([]) ->
  Seeds = application:get_env(barrel, mongo_seeds, {single, "127.0.0.1:27017"}),
  DbName = application:get_env(barrel, mongo_database, db_name()),
  TopologyOpts = application:get_env(barrel, mongo_topology_options, []),
  WorkerOpts0 = application:get_env(barrel, mongo_worker_options, []),
  WorkerOpts = [{database, DbName}] ++ WorkerOpts0,
  ?LOG_INFO("connecting mongo database=~p~n", [DbName]),
  {ok, Topology} = mongoc:connect(Seeds, TopologyOpts, WorkerOpts),
  ?LOG_INFO("mongo database=~p connected~n", [DbName]),
  ok = persistent_term:put({?MODULE, topology}, Topology),
  {ok, Topology}.


handle_call(_Msg, _From, Topology) ->
  {reply, ok, Topology}.

handle_cast(_Msg, Topology) ->
  {noreply, Topology}.

terminate(Reason, Topology) ->
  DbName = barrel_config:get(mongo_database, db_name()),
  ?LOG_INFO("disconnecting mongo databse=~p reason=~p~n", [DbName, Reason]),
  mongoc:disconnect(Topology).
